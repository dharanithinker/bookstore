﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Services.Contracts
{
    public interface IDataAccessLayer
    {
        List<User> GetAllUsers();
        List<Book> GetAllBooks();
        List<Category> GetAllCategories();
        List<OrderDetails> GetOrders(int userDetailsID);
        bool SaveOrder(int userDetailsID, OrderDetails orderDetails);
    }
}
