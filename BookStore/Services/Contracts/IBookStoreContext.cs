﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Services.Contracts
{
    public interface IBookStoreContext
    {
        List<User> Users { get; set; }
        List<UserOrderDetails> UserOrders { get; set; }
        List<Book> Books { get; set; }
        List<Category> Categories { get; set; }

    }
}
