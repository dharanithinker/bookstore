﻿using BookStore.Entities;
using BookStore.Helpers;
using BookStore.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Services.Implementation
{
    public class BookService : IBookService
    {
        IDataAccessLayer dataAccessLayer;
        public BookService(IDataAccessLayer _dataAccessLayer)
        {
            dataAccessLayer = _dataAccessLayer;
        }
        public List<Category> GetAllCategories()
        {
            return dataAccessLayer.GetAllCategories();
        }

        public List<Book> GetAllBooks()
        {
            return dataAccessLayer.GetAllBooks();
        }
    }
}
