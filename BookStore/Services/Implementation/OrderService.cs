﻿using BookStore.Entities;
using BookStore.Helpers;
using BookStore.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Services.Implementation
{
    public class OrderService : IOrderService
    {
        IDataAccessLayer dataAccessLayer;
        public OrderService(IDataAccessLayer _dataAccessLayer)
        {
            dataAccessLayer = _dataAccessLayer;
        }
        public List<OrderDetails> GetOrders(int userDetailsID)
        {
            return dataAccessLayer.GetOrders(userDetailsID);
        }

        public bool SaveOrder(int userDetailsID, OrderDetails orderDetails)
        {
            return dataAccessLayer.SaveOrder(userDetailsID, orderDetails);
        }
    }
}
