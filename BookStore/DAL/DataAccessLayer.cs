﻿using BookStore.Entities;
using BookStore.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DAL
{
    public class DataAccessLayer : IDataAccessLayer
    {
        IBookStoreContext bookstoreContext;
        public DataAccessLayer(IBookStoreContext _bookstoreContext)
        {
            bookstoreContext = _bookstoreContext;
        }

        public List<User> GetAllUsers()
        {
            return bookstoreContext.Users;
        }

        public List<Book> GetAllBooks()
        {
            return bookstoreContext.Books;
        }
        public List<Category> GetAllCategories()
        {
            return bookstoreContext.Categories;
        }

        public List<OrderDetails> GetOrders(int userDetailsID)
        {
            List<OrderDetails> userOrderDetails = new List<OrderDetails>();
            var orderInfo = bookstoreContext.UserOrders
                .Where(x => x.UserDetails.Id == userDetailsID).ToList();
            if (orderInfo != null)
                userOrderDetails = orderInfo.First().OrderDetails;
            return userOrderDetails;
        }

        public bool SaveOrder(int userDetailsID, OrderDetails orderDetails)
        {
            var userDetails = bookstoreContext.Users
                .Where(x => x.Id == userDetailsID).FirstOrDefault();
            orderDetails.OrderedDate = DateTime.UtcNow;
            orderDetails.OrderStatus = Status.Success;
            UserOrderDetails userOrderDetails = bookstoreContext.UserOrders
                .SingleOrDefault(x => x.UserDetails.Equals(userDetails));
            userOrderDetails.OrderDetails.Add(orderDetails);
            //UserOrderDetails userOrderDetails = new UserOrderDetails()
            //{
            //    UserDetails = userDetails,
            //    // OrderDetails = new List<OrderDetails> { orderDetails }
            //    OrderDetails = new List<OrderDetails> { bookstoreContext.UserOrders[0].OrderDetails[0] }
            //};
            //bookstoreContext.UserOrders.Add(userOrderDetails);
            return true;
        }
    }
}

