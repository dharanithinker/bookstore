﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Entities
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string CoverPicture { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public float Price { get; set; }
        public float Discount { get; set; }
        public Category Category { get; set; }
        public int TotalQuantity { get; }

        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;

            Book other = obj as Book;
            if ((Object)other == null)
                return false;

            // here you need to compare two objects
            // below is just example implementation

            return this.Author == other.Author
                && this.Category.Equals(other.Category)
                && this.CoverPicture == other.CoverPicture
                &&this.Description == other.Description
                &&this.Discount ==other.Discount
                &&this.Id == other.Id
                && this.Price == other.Price
                && this.SubTitle == other.SubTitle
                && this.Title == other.Title
                && this.TotalQuantity == other.TotalQuantity;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }


}
