import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Book } from "../_models";
import { Store } from "@ngrx/store";
import { BooksState } from "../state/dashboard.state";

@Component({
  selector: "app-cart",
  templateUrl: "./cart.component.html",
  styleUrls: ["./cart.component.css"]
})
export class CartComponent implements OnInit {
  cartItems: Book[] = [];
  finalAmount = 0;
  constructor(private store: Store<{ books: BooksState }>) {
    let element = JSON.parse(localStorage.getItem("_myCart"));
    if (element) {
      this.cartItems.push(element);
    }

    if (this.cartItems) {
      this.cartItems.forEach(item => {
        this.finalAmount +=
          item.price * item.orderQuantity -
          item.price * item.orderQuantity * (item.discount / 100);
      });
    }
  }

  updateTotalCost() {
    this.finalAmount = 0;
    this.cartItems.forEach(item => {
      this.finalAmount +=
        item.price * item.orderQuantity -
        item.price * item.orderQuantity * (item.discount / 100);
    });
  }
  ngOnInit() {}
}
