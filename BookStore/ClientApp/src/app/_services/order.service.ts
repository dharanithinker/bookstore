import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class OrderService {
  constructor(private http: HttpClient) {}
  getOrders() {
    return this.http.get<any>(`${environment.apiUrl}/api/Order/GetOrders`);
  }
}
