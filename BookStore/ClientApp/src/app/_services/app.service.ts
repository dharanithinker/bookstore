﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment } from "../../environments/environment";
import { User, Book } from "../_models";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({ providedIn: "root" })
export class AppService {
  private cartItemSubject: BehaviorSubject<number>;
  public cartItemCount: Observable<number>;

  constructor(private http: HttpClient) {
    this.cartItemSubject = new BehaviorSubject<number>(0);
    this.cartItemCount = this.cartItemSubject.asObservable();
  }

  setCartItemCount(updatedValue: number) {
    this.cartItemSubject.next(updatedValue);
  }
}
