import { Component, OnInit } from "@angular/core";
import { OrderService } from "../_services/Order.service";

@Component({
  selector: "app-my-orders",
  templateUrl: "./my-orders.component.html",
  styleUrls: ["./my-orders.component.css"]
})
export class MyOrdersComponent implements OnInit {
  orders: any = [];
  constructor(private orderService: OrderService) {}

  ngOnInit() {
    this.orderService.getOrders().subscribe(orderDetails => {
      orderDetails.forEach(order => {
        order.status = this.getOrderStatus(order.orderStatus);
        this.orders.push(order);
      });
    });
  }

  getOrderStatus(orderStaus: number) {
    let computedStatus = "";
    if (orderStaus === 1) {
      computedStatus = "Success";
    } else if (orderStaus === 2) {
      computedStatus = "Dispatched";
    } else if (orderStaus === 3) {
      computedStatus = "Delivered";
    } else if (orderStaus === 4) {
      computedStatus = "Completed";
    } else if (orderStaus === 5) {
      computedStatus = "Cancelled";
    }
    return computedStatus;
  }
}
