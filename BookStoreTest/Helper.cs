﻿using BookStore.Controllers;
using BookStore.DAL;
using BookStore.Entities;
using BookStore.Services.Contracts;
using BookStore.Services.Implementation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace BookStoreTest
{
    public class Helper
    {
        private IBookStoreContext _bookStoreMockContext;
        public IBookStoreContext BookStoreContext
        {
            get
            {
                if (_bookStoreMockContext == null)
                    ArrangeBookStoreContext();
                return _bookStoreMockContext;
            }
        }

        //public IBookStoreContext GetStoreContext()
        //{

        //}

        public void SetUserIdentity(ControllerBase controller)
        {
            var fakeUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, "1"),
                }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = fakeUser }
            };
        }
        public BooksController ArrangeBooksController(bool isHTTPContextRequired, string userID)
        {
            IDataAccessLayer dataAccessLayer = GetDataAccessLayerMock();
            IBookService bookService = new Mock<BookService>(dataAccessLayer).Object;
            BooksController booksController = new BooksController(bookService);
            if (isHTTPContextRequired)
                booksController.ControllerContext = MockControllerContext(userID);
            return booksController;
        }

        public OrderController ArrangeOrderController(bool isHTTPContextRequired, string userID)
        {
            IDataAccessLayer dataAccessLayer = GetDataAccessLayerMock();
            IOrderService orderService = new Mock<OrderService>(dataAccessLayer).Object;
            OrderController orderController = new OrderController(orderService);
            if (isHTTPContextRequired)
               orderController.ControllerContext = MockControllerContext(userID);
            return orderController;
        }

        private IDataAccessLayer GetDataAccessLayerMock()
        {
            IDataAccessLayer dataAccessLayer = new Mock<DataAccessLayer>(BookStoreContext).Object;
            return dataAccessLayer;
        }

        private ControllerContext MockControllerContext(string userID)
        {
            var context = new Mock<HttpContext>();
            var user = new Mock<ClaimsPrincipal>();
            var identity = new Mock<IIdentity>();
            context.Setup(c => c.User).Returns(user.Object);
            user.Setup(c => c.Identity).Returns(identity.Object);
            identity.Setup(i => i.IsAuthenticated).Returns(true);
            identity.Setup(i => i.Name).Returns(userID);
            var _controllerContext = new ControllerContext() { HttpContext = context.Object };
            return _controllerContext;
        }


        private void ArrangeBookStoreContext()
        {
            var bookStoreContext = new Mock<IBookStoreContext>();
            ArrangeBooks(bookStoreContext);
            ArrangeCategories(bookStoreContext);
            ArrangeUsers(bookStoreContext);
            ArrangeUserOrders(bookStoreContext);
            _bookStoreMockContext = bookStoreContext.Object;
        }

        private void ArrangeBooks(Mock<IBookStoreContext> bookStoreContext)
        {
            bookStoreContext
                .Setup(x => x.Books).Returns(
                new List<Book>() {
                    new Book {
                Id = 1,
                Title = "Steve Jobs: The Exclusive Biography",
                SubTitle = "",
                CoverPicture = "https://images-na.ssl-images-amazon.com/images/I/41LcuCqSeJL._SX317_BO1,204,203,200_.jpg",
                Author = " Walter Isaacson ", Description = "This is a riveting book, with as much to say about the transformation of modern life in the information age as about its supernaturally gifted and driven subject",
                Price = 500f,
                Category = new Category() { Id = 1,Name = "Biography" },
                Discount = 0
            }
                }
                            );
        }
        private void ArrangeCategories(Mock<IBookStoreContext> bookStoreContext)
        {
            bookStoreContext
                .Setup(x => x.Categories).Returns(
                new List<Category>() {
                   new Category { Id = 1, Name = "Biography"}
                }
                            );
        }
        private void ArrangeUsers(Mock<IBookStoreContext> bookStoreContext)
        {
            bookStoreContext
                .Setup(x => x.Users).Returns(
              new List<User>
        {
            new User { Id = 1, FirstName = "Dharani", LastName = "Kumar", Username = "dharanithinker_007", Password = "dharanithinker_007" },
        }
                            );
        }

        private void ArrangeUserOrders(Mock<IBookStoreContext> bookStoreContext)
        {
            bookStoreContext
                .Setup(x => x.UserOrders).Returns(
              new List<UserOrderDetails>
        {
                  new UserOrderDetails{
                  UserDetails = new User { Id = 1, FirstName = "Dharani", LastName = "Kumar", Username = "dharanithinker_007", Password = "dharanithinker_007" },
                  OrderDetails = new List<OrderDetails>()
                        {

                        } } }
                            );
        }
    }
}
