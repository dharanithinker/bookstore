﻿using BookStore.Controllers;
using BookStore.DAL;
using BookStore.Entities;
using BookStore.Services.Contracts;
using BookStore.Services.Implementation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStoreTest
{
    [TestClass]
    public class BookControllerTests
    {
        public readonly Helper helperObj = new Helper();

        [TestMethod]
        public void Test_Get_All_Book_Equalto_Mock()
        {
            //Arrange
            BooksController booksController = helperObj.ArrangeBooksController(false,string.Empty);
            List<Book> mockBookCollection = helperObj.BookStoreContext.Books;
            //Act
            var actualBooksCollection = (booksController.GetAllBooks() as OkObjectResult)
                .Value as List<Book>;
            //Assert
            CollectionAssert.AreEqual(mockBookCollection, actualBooksCollection);
        }

        [TestMethod]
        public void Test_Get_All_Category_Equalto_Mock()
        {
            //Arrange
            BooksController booksController = helperObj.ArrangeBooksController(false, string.Empty);
            List<Category> mockCategoryCollection = helperObj.BookStoreContext.Categories;
            //Act
            var actualCategoryCollection = (booksController.GetAllCategories() as OkObjectResult)
                .Value as List<Category>;
            //Assert
            CollectionAssert.AreEqual(mockCategoryCollection, actualCategoryCollection);
        }

        
    }
}
