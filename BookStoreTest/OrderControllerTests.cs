using BookStore.Controllers;
using BookStore.DAL;
using BookStore.Entities;
using BookStore.Services.Contracts;
using BookStore.Services.Implementation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace BookStoreTest
{
    [TestClass]
    public class OrderControllerTests
    {
        public readonly Helper helperObj = new Helper();
        [TestMethod]
        public void Test_Save_Order_Asserting_With_MockOrder()
        {
            //Arrange
            OrderController orderController = helperObj.ArrangeOrderController(true, "1");
            OrderDetails orderDetails =
                    new OrderDetails()
                    {
                        Id = 15,
                        Items = new List<Item>{
                            new Item() { OrderedQuantity = 3, Book =
                            new Book {
                Id = 2,
                Title = "The Text: A short story",
                SubTitle = "",
                CoverPicture = "https://images-eu.ssl-images-amazon.com/images/I/51JckI5WcOL.jpg",
                Author = "Claire Douglas",
                Description = "A new short story from the Sunday Times bestselling author of Local Girl Missing and Last Seen Alive. Her newest novel, Do Not Disturb, is out now!",
                Price = 650f,
                Category = new Category(){ Id = 2, Name = "Crime" } ,
                Discount = 10
            }
                            },
                        },
                        OrderStatus = Status.Delivered,
                        OrderedDate = DateTime.UtcNow.AddDays(-20),
                        DeliveredDate = DateTime.UtcNow.AddDays(-3)

                    };
            //Act
            orderController.Order(orderDetails);
            List<OrderDetails> userOrderCollection = (orderController.GetOrders() as OkObjectResult)
                .Value as List<OrderDetails>;
            //Assert
            Assert.IsTrue(userOrderCollection.Any(x => x.Equals(orderDetails)));
        }

        [TestMethod]
        public void Test_Get_Order_Asserting_With_MockOrder()
        {
            //Arrange
            OrderController orderController = helperObj.ArrangeOrderController(true,"1");
            OrderDetails mockOrderDetails =
                    new OrderDetails()
                    {
                        Id = 15,
                        Items = new List<Item>{
                            new Item() { OrderedQuantity = 3, Book =
                            new Book {
                Id = 2,
                Title = "The Text: A short story",
                SubTitle = "",
                CoverPicture = "https://images-eu.ssl-images-amazon.com/images/I/51JckI5WcOL.jpg",
                Author = "Claire Douglas",
                Description = "A new short story from the Sunday Times bestselling author of Local Girl Missing and Last Seen Alive. Her newest novel, Do Not Disturb, is out now!",
                Price = 650f,
                Category = new Category(){ Id = 2, Name = "Crime" } ,
                Discount = 10
            }
                            },
                        },
                        OrderStatus = Status.Delivered,
                        OrderedDate = DateTime.UtcNow.AddDays(-20),
                        DeliveredDate = DateTime.UtcNow.AddDays(-3)

                    };
            orderController.Order(mockOrderDetails);
            var mockOrderCollection = new List<OrderDetails>() { mockOrderDetails};
            //Act
            List<OrderDetails> actualOrderCollection = (orderController.GetOrders() as OkObjectResult)
                .Value as List<OrderDetails>;
            //Assert
            CollectionAssert.AreEqual(mockOrderCollection, actualOrderCollection);
        }
    }
}
